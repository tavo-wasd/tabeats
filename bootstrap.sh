#!/bin/sh

(cd "$( dirname -- "$0" )" ;
    python -m venv ./ ;
    . ./bin/activate ;
    pip install discord ;
    pip install PyNaCl ;
    pip install python-dotenv ;
    pip install ytmusicapi ;
    pip install yt_dlp ;
)
